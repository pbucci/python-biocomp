<h4>Ejercicio 1:</h4>

Una secuencia (**ATGGAAGTTAGG**) dentro de un gen X tiende a sufrir mutaciones cada vez que es expuesto a un agente radiactivo, el cual logra cambiar una "T" (timina) por una "A" (adenina).

1) Escribir un programa que cambie todas las T luego de una sola exposición

2) Escribir un programa que muestre el cambio solo de **dos** bases "T".

